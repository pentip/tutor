/*
 * Copyright 2021-2022 the original author and authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.edu.tjnu.tutor.system.structure;

import cn.edu.tjnu.tutor.system.domain.dto.ApplyDTO;
import cn.edu.tjnu.tutor.system.domain.dto.GroupDTO;
import cn.edu.tjnu.tutor.system.domain.entity.Group;
import cn.edu.tjnu.tutor.system.domain.extra.UserGroup;
import cn.edu.tjnu.tutor.system.domain.view.GroupVO;
import cn.edu.tjnu.tutor.system.settings.MapstructSettings;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDateTime;

/**
 * 导师小组实体类结构映射接口。
 *
 * @author 王帅
 * @since 2.0
 */
@Mapper(config = MapstructSettings.class, imports = LocalDateTime.class)
public interface GroupStruct {

    /**
     * 转换为视图对象。
     *
     * @param group 实体类对象
     * @return 视图对象
     */
    GroupVO toVO(Group group);

    /**
     * 转换到实体类对象。
     *
     * @param dto 数据传输对象
     * @return 实体类对象
     */
    @Mapping(target = "type", ignore = true)
    @Mapping(target = "userId", ignore = true)
    @Mapping(target = "captainId", ignore = true)
    @Mapping(target = "stock", source = "total")
    Group toEntity(GroupDTO dto);

    /**
     * 转换到实体类对象。
     *
     * @param dto    数据传输对象
     * @param userId 用户主键
     * @param type   导师小组类型
     * @return 实体类对象
     */
    @Mapping(target = "captainId", ignore = true)
    @Mapping(target = "stock", source = "dto.total")
    Group toEntity(GroupDTO dto, Integer userId, String type);

    /**
     * 转换到实体类对象。
     *
     * @param dto    数据传输对象
     * @param userId 用户主键
     * @return 实体类对象
     */
    @Mapping(target = "type", constant = "1")
    @Mapping(target = "applyTime", expression = "java(LocalDateTime.now())")
    UserGroup toEntity(ApplyDTO dto, Integer userId);

}