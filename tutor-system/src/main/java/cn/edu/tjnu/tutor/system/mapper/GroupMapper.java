/*
 * Copyright 2021-2022 the original author and authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.edu.tjnu.tutor.system.mapper;

import cn.edu.tjnu.tutor.common.cache.MybatisRedisCache;
import cn.edu.tjnu.tutor.system.domain.entity.Group;
import cn.edu.tjnu.tutor.system.domain.view.GroupApplyVO;
import cn.edu.tjnu.tutor.system.domain.view.GroupReplyVO;
import cn.edu.tjnu.tutor.system.domain.view.GroupVO;
import cn.edu.tjnu.tutor.system.domain.view.UserVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;

/**
 * 导师小组信息数据层。
 *
 * @author 王帅
 * @since 2.0
 */
@CacheNamespace(implementation = MybatisRedisCache.class, eviction = MybatisRedisCache.class)
public interface GroupMapper extends BaseMapper<Group> {

    /**
     * 分页查询导师小组信息。
     *
     * @param <P>    分页对象类型
     * @param userId 用户主键
     * @param page   分页参数
     * @return 分页对象
     */
    <P extends IPage<GroupVO>> P selectPageVO(@Param("userId") Integer userId, P page);

    /**
     * 分页查询导师小组成员信息。
     *
     * @param groupId 小组主键
     * @param page    分页参数
     * @return 分页对象
     */
    <P extends IPage<UserVO>> P selectPageUserVO(@Param("groupId") Integer groupId, P page);

    /**
     * 分页查询导师小组申请信息。
     *
     * @param <P>    分页对象类型
     * @param userId 用户主键
     * @param page   分页参数
     * @return 分页对象
     */
    <P extends IPage<GroupApplyVO>> P selectPageApplyVO(@Param("userId") Integer userId, P page);

    /**
     * 分页查询已加入导师小组信息。
     *
     * @param <P>    分页对象类型
     * @param userId 用户主键
     * @param page   分页参数
     * @return 分页对象
     */
    <P extends IPage<GroupReplyVO>> P selectPageReplyVO(@Param("userId") Integer userId, P page);

    /**
     * 查询学生是否在导师小组内。
     *
     * @param teacherId 教师用户主键
     * @param studentId 学生用户主键
     * @return {@code 0} 不在小组内，{@code 1} 在小组内
     */
    int selectUserCount(@Param("teacherId") Integer teacherId,
                        @Param("studentId") Integer studentId);

}