/*
 * Copyright 2021-2022 the original author and authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.edu.tjnu.tutor.system.domain.extra;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户（学生、导师）和导师小组关联。
 *
 * @author 王帅
 * @since 2.0
 */
@Data
@TableName("ref_user_group")
public class UserGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户主键。
     */
    @TableId(type = IdType.AUTO)
    private Integer userId;

    /**
     * 小组主键。
     */
    private Integer groupId;

    /**
     * 绑定类型：0 绑定成功，1 申请成功。
     */
    private Integer type;

    /**
     * 申请时间。
     */
    private LocalDateTime applyTime;

    /**
     * 申请理由。
     */
    private String reason;

}