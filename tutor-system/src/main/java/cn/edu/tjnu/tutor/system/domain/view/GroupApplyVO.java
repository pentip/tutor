/*
 * Copyright 2021-2022 the original author and authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.edu.tjnu.tutor.system.domain.view;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 小组申请信息。
 *
 * @author 王帅
 * @since 2.0
 */
@Data
public class GroupApplyVO implements Serializable {

    private static final long serialVersionUID = -3558790298695905335L;

    /**
     * 申请人主键。
     *
     * @mock 1
     */
    private Integer userId;

    /**
     * 小组主键。
     *
     * @mock 1
     */
    private Integer groupId;

    /**
     * 申请人名称。
     *
     * @mock 王帅
     */
    private String userName;

    /**
     * 申请人学号。
     *
     * @mock 1940050005
     */
    private String userCode;

    /**
     * 小组名称。
     *
     * @mock 第一实习小组
     */
    private String groupName;

    /**
     * 申请人所在学院名称。
     *
     * @mock 化学学院
     */
    private String collegeName;

    /**
     * 申请时间。
     *
     * @mock 2022-10-01
     */
    private LocalDateTime applyTime;

    /**
     * 申请理由。
     *
     * @mock 我想进入小组
     */
    private String reason;

}