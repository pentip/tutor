/*
 * Copyright 2021-2022 the original author and authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.edu.tjnu.tutor.system.service;

import cn.edu.tjnu.tutor.system.domain.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 角色信息服务层。
 *
 * @author 王帅
 * @since 1.0
 */
public interface RoleService extends IService<Role> {

    /**
     * 根据用户主键获取用户的角色权限。
     *
     * @param userId 用户主键
     * @return 角色信息
     */
    List<Role> getRoles(Integer userId);

    /**
     * 判断所给用户主键对应的用户是否为指定角色。
     *
     * @param userId  用户主键
     * @param roleKey 角色键名
     * @return {@code true} 用户为指定角色，{@code false} 用户非指定角色
     */
    boolean hasRole(Integer userId, String roleKey);

    /**
     * 绑定用户与角色。
     *
     * @param userId  用户主键
     * @param roleKey 角色键名
     * @return {@code true} 绑定成功，{@code false} 绑定失败
     */
    boolean bindRole(Integer userId, String roleKey);

    /**
     * 删除用户与角色。
     *
     * @param userId  用户主键
     * @param roleKey 角色键名
     * @return {@code true} 解除成功，{@code false} 解除失败
     */
    boolean dumpRole(Integer userId, String roleKey);

}