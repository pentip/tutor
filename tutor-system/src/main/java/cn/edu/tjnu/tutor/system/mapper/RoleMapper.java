/*
 * Copyright 2021-2022 the original author and authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.edu.tjnu.tutor.system.mapper;

import cn.edu.tjnu.tutor.common.cache.MybatisRedisCache;
import cn.edu.tjnu.tutor.system.domain.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色信息数据层。
 *
 * @author 王帅
 * @since 1.0
 */
@CacheNamespace(implementation = MybatisRedisCache.class, eviction = MybatisRedisCache.class)
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 查找用户对应的角色信息。
     *
     * @param userId 用户主键
     * @return 所有角色
     */
    List<Role> selectRolesByUserId(Integer userId);

    /**
     * 判断用户是否具备该角色。
     *
     * @param userId  用户主键
     * @param roleKey 角色键名
     * @return {@code 1} 具有该角色，{@code 0} 不具有该角色
     */
    int hasRole(@Param("userId") Integer userId, @Param("roleKey") String roleKey);

    /**
     * 为用户创建关联角色。
     *
     * @param userId  用户主键
     * @param roleKey 角色键名
     * @return {@code 1} 绑定成功，{@code 0} 绑定失败
     */
    int bindRole(@Param("userId") Integer userId, @Param("roleKey") String roleKey);

    /**
     * 删除用户与角色的绑定。
     *
     * @param userId  用户主键
     * @param roleKey 角色键名
     * @return {@code 1} 删除成功，{@code 0} 删除失败
     */
    int dumpRole(@Param("userId") Integer userId, @Param("roleKey") String roleKey);

}