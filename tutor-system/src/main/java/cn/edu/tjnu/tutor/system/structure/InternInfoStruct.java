/*
 * Copyright 2021-2022 the original author and authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.edu.tjnu.tutor.system.structure;

import cn.edu.tjnu.tutor.system.domain.dto.InternInfoDTO;
import cn.edu.tjnu.tutor.system.domain.entity.InternInfo;
import cn.edu.tjnu.tutor.system.domain.view.InternInfoVO;
import cn.edu.tjnu.tutor.system.settings.MapstructSettings;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * 教育实习基本信息实体类结构映射接口。
 *
 * @author 王帅
 * @since 2.0
 */
@Mapper(config = MapstructSettings.class)
public interface InternInfoStruct {

    /**
     * 转换为视图对象。
     *
     * @param entity 实体类对象
     * @return 视图对象
     */
    InternInfoVO toVO(InternInfo entity);

    /**
     * 转换到实体类对象。
     *
     * @param dto 数据传输对象
     * @return 实体类对象
     */
    @Mapping(target = "infoId", ignore = true)
    @Mapping(target = "userId", ignore = true)
    @Mapping(target = "schRemark", ignore = true)
    @Mapping(target = "univRemark", ignore = true)
    @Mapping(target = "schLessonPlan", ignore = true)
    @Mapping(target = "schLectureNote", ignore = true)
    @Mapping(target = "schTeacherMoral", ignore = true)
    @Mapping(target = "schTeachingStudy", ignore = true)
    @Mapping(target = "univLessonPlan", ignore = true)
    @Mapping(target = "univLectureNote", ignore = true)
    @Mapping(target = "univTeacherMoral", ignore = true)
    @Mapping(target = "univTeachingStudy", ignore = true)
    @Mapping(target = "personalSummary", ignore = true)
    @Mapping(target = "groupEvaluation", ignore = true)
    @Mapping(target = "groupScore", ignore = true)
    InternInfo toEntity(InternInfoDTO dto);

}