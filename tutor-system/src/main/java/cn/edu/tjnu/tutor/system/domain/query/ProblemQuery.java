/*
 * Copyright 2021-2022 the original author and authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.edu.tjnu.tutor.system.domain.query;

import cn.edu.tjnu.tutor.common.core.domain.query.PageQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 问题信息查询对象。
 *
 * @author 王帅
 * @since 2.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProblemQuery extends PageQuery {

    private static final long serialVersionUID = 2531941971831029485L;

    /**
     * 提问者姓名。
     */
    private String questioner;

    /**
     * 发布问题者所属学院名称。
     */
    private String collegeName;

    /**
     * 问题标题。
     *
     * @mock 这是第一个问题
     */
    private String title;

    /**
     * 可见范围（0 导师小组可见，1 班级内可见，2 精选问题/公开）。
     */
    private Integer scope;

    /**
     * 当前状态（0 未解决，1 线上解决，2 线下解决）。
     */
    private Integer status;

}