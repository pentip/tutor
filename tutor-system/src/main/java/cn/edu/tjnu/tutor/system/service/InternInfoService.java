/*
 * Copyright 2021-2022 the original author and authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.edu.tjnu.tutor.system.service;

import cn.edu.tjnu.tutor.system.domain.entity.InternInfo;
import cn.edu.tjnu.tutor.system.domain.view.InternInfoVO;
import cn.edu.tjnu.tutor.system.domain.view.RemarkVO;
import cn.edu.tjnu.tutor.system.domain.view.ReportVO;
import cn.edu.tjnu.tutor.system.domain.view.ScoreVO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 教育实习基本信息服务层。
 *
 * @author 王帅
 * @since 2.0
 */
public interface InternInfoService extends IService<InternInfo> {

    /**
     * 实习教案。
     */
    String LESSON_PLAN = "JA";

    /**
     * 听课记录。
     */
    String LECTURE_NOTE = "TK";

    /**
     * 师德表现。
     */
    String TEACHER_MORAL = "SD";

    /**
     * 教研活动。
     */
    String TEACHING_STUDY = "JY";

    /**
     * 中学教师。
     */
    String SCH = "ZX";

    /**
     * 高校教师。
     */
    String UNIV = "GX";

    /**
     * 个人总结。
     */
    String PERSON = "GR";

    /**
     * 获取实习生基本信息。
     *
     * @param userId 实习生主键
     * @return 实习生基本信息
     */
    InternInfoVO getInfo(Integer userId);

    /**
     * 获取实习生成绩信息。
     *
     * @param userId 实习生主键
     * @return 成绩报告
     */
    ReportVO getReport(Integer userId);

    /**
     * 得到分数
     *
     * @param userId 用户id
     * @param type   类型
     * @return {@link ScoreVO}
     */
    ScoreVO getScore(Integer userId, String type);

    /**
     * 组得分
     *
     * @param userId    用户id
     * @param type      类型
     * @param schScore  原理图分数
     * @param univScore 大学分数
     * @return boolean
     */
    boolean setScore(Integer userId, String type, Integer schScore, Integer univScore);

    /**
     * 得到评论
     *
     * @param userId 用户id
     * @return {@link RemarkVO}
     */
    RemarkVO getRemark(Integer userId);

    /**
     * 设置评论
     *
     * @param userId  用户id
     * @param type    类型
     * @param content 内容
     * @return boolean
     */
    default boolean setRemark(Integer userId, String type, String content) {
        return lambdaUpdate()
                .set(SCH.equals(type), InternInfo::getSchRemark, content)
                .set(UNIV.equals(type), InternInfo::getUnivRemark, content)
                .set(PERSON.equals(type), InternInfo::getPersonalSummary, content)
                .eq(InternInfo::getUserId, userId)
                .update();
    }

}