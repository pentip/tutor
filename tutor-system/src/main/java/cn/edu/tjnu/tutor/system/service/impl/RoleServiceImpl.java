/*
 * Copyright 2021-2022 the original author and authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.edu.tjnu.tutor.system.service.impl;

import cn.edu.tjnu.tutor.system.domain.entity.Role;
import cn.edu.tjnu.tutor.system.mapper.RoleMapper;
import cn.edu.tjnu.tutor.system.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

import static cn.edu.tjnu.tutor.common.util.SqlUtils.toBool;

/**
 * 角色信息服务层实现。
 *
 * @author 王帅
 * @since 1.0
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Override
    public List<Role> getRoles(Integer userId) {
        return baseMapper.selectRolesByUserId(userId);
    }

    @Override
    public boolean hasRole(Integer userId, String roleKey) {
        return toBool(baseMapper.hasRole(userId, roleKey));
    }

    @Override
    public boolean bindRole(Integer userId, String roleKey) {
        // 如果用户没有绑定角色则绑定这个角色
        return !toBool(baseMapper.hasRole(userId, roleKey)) &&
                toBool(baseMapper.bindRole(userId, roleKey));
    }

    @Override
    public boolean dumpRole(Integer userId, String roleKey) {
        return toBool(baseMapper.dumpRole(userId, roleKey));
    }

}