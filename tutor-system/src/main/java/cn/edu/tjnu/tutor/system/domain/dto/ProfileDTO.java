/*
 * Copyright 2021-2022 the original author and authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.edu.tjnu.tutor.system.domain.dto;

import cn.edu.tjnu.tutor.common.validation.constraints.Phone;
import cn.edu.tjnu.tutor.common.validation.constraints.QQ;
import cn.edu.tjnu.tutor.common.validation.constraints.WeChat;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import java.io.Serializable;

/**
 * 用户基本信息数据传输对象。
 *
 * @author 王帅
 * @since 2.0
 */
@Data
public class ProfileDTO implements Serializable {

    private static final long serialVersionUID = -4540242312656742488L;

    /**
     * QQ号。
     *
     * @mock 6685432435
     */
    @QQ
    private String qq;

    /**
     * 微信号。
     *
     * @mock KKT876hv4
     */
    @WeChat
    private String wechat;

    /**
     * 用户邮箱。
     *
     * @mock chemistry@email.com
     */
    @Email
    private String email;

    /**
     * 手机号码。
     *
     * @mock 13000000001
     */
    @Phone
    private String phone;

    /**
     * 用户性别（0女，1男，2保密）。
     *
     * @mock 1
     */
    @Range(max = 1L)
    private Integer gender;

    /**
     * 头像地址。
     *
     * @mock ""
     */
    private String avatar;

    /**
     * 自我介绍。
     *
     * @mock 天津师范大学化学学院学生
     */
    private String introduction;

}