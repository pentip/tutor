/*
 * Copyright 2021-2022 the original author and authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.edu.tjnu.tutor.system.domain.view;

import lombok.Data;

import java.io.Serializable;

/**
 * 对象存储信息。
 *
 * @author 王帅
 * @since 2.0
 */
@Data
public class OssVO implements Serializable {

    private static final long serialVersionUID = 6705796544652383935L;

    /**
     * 文件名。
     *
     * @mock 2f7c1949e05a4f6d8d33d429c0e66d59
     */
    private String fileName;

    /**
     * 原名。
     *
     * @mock 学习资料.jpg
     */
    private String originalName;

    /**
     * 文件后缀名。
     *
     * @mock jpg
     */
    private String fileSuffix;

    /**
     * URL地址。
     *
     * @mock https://localhost/files/999876
     */
    private String url;

}