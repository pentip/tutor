/*
 * Copyright 2021-2022 the original author and authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.edu.tjnu.tutor.system.domain.dto;

import cn.edu.tjnu.tutor.common.validation.groups.Insert;
import cn.edu.tjnu.tutor.common.validation.groups.Update;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;

/**
 * 问题信息数据传输对象。
 *
 * @author 王帅
 * @since 1.0
 */
@Data
public class ProblemDTO implements Serializable {

    private static final long serialVersionUID = -5517426617356862805L;

    /**
     * 问题主键。
     *
     * @mock 1
     */
    @Null(groups = Update.class)
    @NotNull(groups = Insert.class)
    private String problemId;

    /**
     * 问题标题。
     *
     * @mock 这是第一个问题
     */
    @Length(max = 100)
    @Null(groups = Update.class)
    @NotNull(groups = Insert.class)
    private String title;

    /**
     * 详细内容。
     *
     * @mock 问题的详细信息描述
     */
    @NotNull(groups = Insert.class)
    private String content;

    /**
     * 可见范围（0 导师小组可见，1 班级内可见，2 精选问题/公开）。
     */
    @Range(max = 2)
    @NotNull(groups = Insert.class)
    private Integer scope;

    /**
     * 当前状态（0 未解决，1 线上解决，2 线下解决）。
     */
    private Integer status;

}
