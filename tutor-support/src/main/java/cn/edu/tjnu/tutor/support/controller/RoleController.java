/*
 * Copyright 2021-2022 the original author and authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.edu.tjnu.tutor.support.controller;

import cn.edu.tjnu.tutor.common.core.controller.BaseController;
import cn.edu.tjnu.tutor.common.core.domain.AjaxResult;
import cn.edu.tjnu.tutor.system.domain.entity.Role;
import cn.edu.tjnu.tutor.system.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cn.edu.tjnu.tutor.common.constant.RoleConst.*;
import static cn.edu.tjnu.tutor.common.enums.ExceptionType.ROLE_IS_NOT_ALLOWED;

/**
 * 角色信息控制层。
 *
 * @author 王帅
 * @since 2.0
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/role")
public class RoleController extends BaseController {

    private final RoleService roleService;

    /**
     * 根据用户主键获取用户的权限信息。
     *
     * @param userId 用户主键|1
     */
    @Secured({ROLE_ROOT, ROLE_ADMIN})
    @GetMapping("getRoles/{userId}")
    public List<Role> getRoles(@PathVariable Integer userId) {
        return roleService.getRoles(userId);
    }

    /**
     * 管理员绑定学院管理员角色。
     *
     * @param userId 用户主键|1
     */
    @Secured(ROLE_ROOT)
    @PostMapping("bindAdmin/{userId}")
    public AjaxResult<Void> bindAdmin(@PathVariable Integer userId) {
        if (roleService.hasRole(userId, ROLE_STUDENT)) {
            return error(ROLE_IS_NOT_ALLOWED);
        }
        return toResult(roleService.bindRole(userId, ROLE_ADMIN));
    }

    /**
     * 管理员解除学院管理员角色。
     *
     * @param userId 用户主键|1
     */
    @Secured(ROLE_ROOT)
    @DeleteMapping("dumpAdmin/{userId}")
    public AjaxResult<Void> dumpAdmin(@PathVariable Integer userId) {
        return toResult(roleService.dumpRole(userId, ROLE_ADMIN));
    }

    /**
     * 学院管理员绑定导师角色。
     *
     * @param userId 用户主键|1
     */
    @Secured(ROLE_ADMIN)
    @PostMapping("bindTutor/{userId}")
    public AjaxResult<Void> bindTutor(@PathVariable Integer userId) {
        if (roleService.hasRole(userId, ROLE_STUDENT)) {
            return error(ROLE_IS_NOT_ALLOWED);
        }
        return toResult(roleService.bindRole(userId, ROLE_TUTOR));
    }

    /**
     * 学院管理员解除导师角色。
     *
     * @param userId 用户主键|1
     */
    @Secured(ROLE_ADMIN)
    @DeleteMapping("dumpTutor/{userId}")
    public AjaxResult<Void> dumpTutor(@PathVariable Integer userId) {
        return toResult(roleService.dumpRole(userId, ROLE_TUTOR));
    }

    /**
     * 学院管理员绑定指导老师角色。
     *
     * @param userId 用户主键|1
     */
    @Secured(ROLE_ADMIN)
    @PostMapping("bindInstructor/{userId}")
    public AjaxResult<Void> bindInstructor(@PathVariable Integer userId) {
        if (roleService.hasRole(userId, ROLE_STUDENT)) {
            return error(ROLE_IS_NOT_ALLOWED);
        }
        return toResult(roleService.bindRole(userId, ROLE_INSTRUCTOR));
    }

    /**
     * 学院管理员解除指导老师角色。
     *
     * @param userId 用户主键|1
     */
    @Secured(ROLE_ADMIN)
    @DeleteMapping("dumpInstructor/{userId}")
    public AjaxResult<Void> dumpInstructor(@PathVariable Integer userId) {
        return toResult(roleService.dumpRole(userId, ROLE_INSTRUCTOR));
    }

}